# App
PRODUCT_COPY_FILES += \
	vendor/gnosis/prebuilt/app/AdvancedReboot/AdvancedReboot.apk:system/app/AdvancedReboot/AdvancedReboot.apk \
	vendor/gnosis/prebuilt/app/arcore/arcore.apk:system/app/arcore/arcore.apk \
	vendor/gnosis/prebuilt/app/Calendar/Calendar.apk:system/app/Calendar/Calendar.apk \
	vendor/gnosis/prebuilt/app/Calendar/oat/arm64/Calendar.odex:system/app/Calendar/oat/arm64/Calendar.odex \
	vendor/gnosis/prebuilt/app/Calendar/oat/arm64/Calendar.vdex:system/app/Calendar/oat/arm64/Calendar.vdex \
	vendor/gnosis/prebuilt/app/Camera/Camera.apk:system/app/Camera/Camera.apk \
	vendor/gnosis/prebuilt/app/Chrome/Chrome.apk:system/app/Chrome/Chrome.apk \
	vendor/gnosis/prebuilt/app/DeviceSettings/DeviceSettings.apk:system/app/DeviceSettings/DeviceSettings.apk \
	vendor/gnosis/prebuilt/app/Drive/Drive.apk:system/app/Drive/Drive.apk \
	vendor/gnosis/prebuilt/app/EaselServicePrebuilt/EaselServicePrebuilt.apk:system/app/EaselServicePrebuilt/EaselServicePrebuilt.apk \
	vendor/gnosis/prebuilt/app/FaceLock/FaceLock.apk:system/app/FaceLock/FaceLock.apk \
	vendor/gnosis/prebuilt/app/FaceLock/lib/arm64/libfacenet.so:system/app/FaceLock/lib/arm64/libfacenet.so \
	vendor/gnosis/prebuilt/app/GoogleCalendarSyncAdapter/GoogleCalendarSyncAdapter.apk:system/app/GoogleCalendarSyncAdapter/GoogleCalendarSyncAdapter.apk \
	vendor/gnosis/prebuilt/app/GoogleContactsSyncAdapter/GoogleContactsSyncAdapter.apk:system/app/GoogleContactsSyncAdapter/GoogleContactsSyncAdapter.apk \
	vendor/gnosis/prebuilt/app/GoogleExtShared/GoogleExtShared.apk:system/app/GoogleExtShared/GoogleExtShared.apk \
	vendor/gnosis/prebuilt/app/GoogleTTS/GoogleTTS.apk:system/app/GoogleTTS/GoogleTTS.apk \
	vendor/gnosis/prebuilt/app/GoogleVrCore/GoogleVrCore.apk:system/app/GoogleVrCore/GoogleVrCore.apk \
	vendor/gnosis/prebuilt/app/LatinIMEGooglePrebuilt/LatinIMEGooglePrebuilt.apk:system/app/LatinIMEGooglePrebuilt/LatinIMEGooglePrebuilt.apk \
	vendor/gnosis/prebuilt/app/MarkupGoogle/MarkupGoogle.apk:system/app/MarkupGoogle/MarkupGoogle.apk \
	vendor/gnosis/prebuilt/app/Music2/Music2.apk:system/app/Music2/Music2.apk \
	vendor/gnosis/prebuilt/app/Newsstand/Newsstand.apk:system/app/Newsstand/Newsstand.apk \
	vendor/gnosis/prebuilt/app/Ornament/Ornament.apk:system/app/Ornament/Ornament.apk \
	vendor/gnosis/prebuilt/app/PrebuiltGmail/PrebuiltGmail.apk:system/app/PrebuiltGmail/PrebuiltGmail.apk \
	vendor/gnosis/prebuilt/app/SMSConnectPrebuilt/SMSConnectPrebuilt.apk:system/app/SMSConnectPrebuilt/SMSConnectPrebuilt.apk \
	vendor/gnosis/prebuilt/app/StickersJapan/StickersJapan.apk:system/app/StickersJapan/StickersJapan.apk \
	vendor/gnosis/prebuilt/app/StickersMusic/StickersMusic.apk:system/app/StickersMusic/StickersMusic.apk \
	vendor/gnosis/prebuilt/app/StickersSports/StickersSports.apk:system/app/StickersSports/StickersSports.apk \
	vendor/gnosis/prebuilt/app/talkback/talkback.apk:system/app/talkback/talkback.apk \
	vendor/gnosis/prebuilt/app/TranslatePrebuilt/TranslatePrebuilt.apk:system/app/TranslatePrebuilt/TranslatePrebuilt.apk \
	vendor/gnosis/prebuilt/app/WallpaperPickerGooglePrebuilt/WallpaperPickerGooglePrebuilt.apk:system/app/WallpaperPickerGooglePrebuilt/WallpaperPickerGooglePrebuilt.apk \
	vendor/gnosis/prebuilt/app/WallpapersBReel2017/WallpapersBReel2017.apk:system/app/WallpapersBReel2017/WallpapersBReel2017.apk \
	vendor/gnosis/prebuilt/app/WallpapersBReel2018/WallpapersBReel2018.apk:system/app/WallpapersBReel2018/WallpapersBReel2018.apk \
	vendor/gnosis/prebuilt/app/WebViewStub/WebViewStub.apk:system/app/WebViewStub/WebViewStub.apk \
	vendor/gnosis/prebuilt/app/YouTube/YouTube.apk:system/app/YouTube/YouTube.apk

# Priv-app
PRODUCT_COPY_FILES += \
	vendor/gnosis/prebuilt/priv-app/AndroidMigratePrebuilt/AndroidMigratePrebuilt.apk:system/priv-app/AndroidMigratePrebuilt/AndroidMigratePrebuilt.apk \
	vendor/gnosis/prebuilt/priv-app/AndroidPlatformServices/AndroidPlatformServices.apk:system/priv-app/AndroidPlatformServices/AndroidPlatformServices.apk \
	vendor/gnosis/prebuilt/priv-app/Asdiv/Asdiv.apk:system/priv-app/Asdiv/Asdiv.apk \
	vendor/gnosis/prebuilt/priv-app/ConfigUpdater/ConfigUpdater.apk:system/priv-app/ConfigUpdater/ConfigUpdater.apk \
	vendor/gnosis/prebuilt/priv-app/GCS/GCS.apk:system/priv-app/GCS/GCS.apk \
	vendor/gnosis/prebuilt/priv-app/GoogleBackupTransport/GoogleBackupTransport.apk:system/priv-app/GoogleBackupTransport/GoogleBackupTransport.apk \
	vendor/gnosis/prebuilt/priv-app/GoogleExtServices/GoogleExtServices.apk:system/priv-app/GoogleExtServices/GoogleExtServices.apk \
	vendor/gnosis/prebuilt/priv-app/GoogleFeedback/GoogleFeedback.apk:system/priv-app/GoogleFeedback/GoogleFeedback.apk \
	vendor/gnosis/prebuilt/priv-app/GoogleOneTimeInitializer/GoogleOneTimeInitializer.apk:system/priv-app/GoogleOneTimeInitializer/GoogleOneTimeInitializer.apk \
	vendor/gnosis/prebuilt/priv-app/GooglePartnerSetup/GooglePartnerSetup.apk:system/priv-app/GooglePartnerSetup/GooglePartnerSetup.apk \
	vendor/gnosis/prebuilt/priv-app/GoogleRestore/GoogleRestore.apk:system/priv-app/GoogleRestore/GoogleRestore.apk \
	vendor/gnosis/prebuilt/priv-app/GoogleServicesFramework/GoogleServicesFramework.apk:system/priv-app/GoogleServicesFramework/GoogleServicesFramework.apk \
	vendor/gnosis/prebuilt/priv-app/MatchmakerPrebuilt/MatchmakerPrebuilt.apk:system/priv-app/MatchmakerPrebuilt/MatchmakerPrebuilt.apk \
	vendor/gnosis/prebuilt/priv-app/Phonesky/Phonesky.apk:system/priv-app/Phonesky/Phonesky.apk \
	vendor/gnosis/prebuilt/priv-app/PrebuiltGmsCorePi/PrebuiltGmsCorePi.apk:system/priv-app/PrebuiltGmsCorePi/PrebuiltGmsCorePi.apk \
	vendor/gnosis/prebuilt/priv-app/Recorder/Recorder.apk:system/priv-app/Recorder/Recorder.apk \
	vendor/gnosis/prebuilt/priv-app/Turbo/Turbo.apk:system/priv-app/Turbo/Turbo.apk \
	vendor/gnosis/prebuilt/priv-app/WellbeingPrebuilt/WellbeingPrebuilt.apk:system/priv-app/WellbeingPrebuilt/WellbeingPrebuilt.apk

# ETC
PRODUCT_COPY_FILES += \
	vendor/gnosis/prebuilt/etc/default-permissions/default-permissions.xml:system/etc/default-permissions/default-permissions.xml \
	vendor/gnosis/prebuilt/etc/default-permissions/opengapps-permissions.xml:system/etc/default-permissions/opengapps-permissions.xml \
	vendor/gnosis/prebuilt/etc/permissions/android.hardware.camera.flash-autofocus.xml:system/etc/permissions/android.hardware.camera.flash-autofocus.xml \
	vendor/gnosis/prebuilt/etc/permissions/android.hardware.camera.front.xml:system/etc/permissions/android.hardware.camera.front.xml \
	vendor/gnosis/prebuilt/etc/permissions/android.hardware.camera.full.xml:system/etc/permissions/android.hardware.camera.full.xml \
	vendor/gnosis/prebuilt/etc/permissions/android.hardware.camera.raw.xml:system/etc/permissions/android.hardware.camera.raw.xml \
	vendor/gnosis/prebuilt/etc/permissions/android.hardware.vr.headtracking.xml:system/etc/permissions/android.hardware.vr.headtracking.xml \
	vendor/gnosis/prebuilt/etc/permissions/android.hardware.vr.high_performance.xml:system/etc/permissions/android.hardware.vr.high_performance.xml \
	vendor/gnosis/prebuilt/etc/permissions/android.software.vr.mode.xml:system/etc/permissions/android.software.vr.mode.xml \
	vendor/gnosis/prebuilt/etc/permissions/com.google.android.camera.experimental2016.xml:system/etc/permissions/com.google.android.camera.experimental2016.xml \
	vendor/gnosis/prebuilt/etc/permissions/com.google.android.dialer.support.xml:system/etc/permissions/com.google.android.dialer.support.xml \
	vendor/gnosis/prebuilt/etc/permissions/com.google.android.hardwareinfo.xml:system/etc/permissions/com.google.android.hardwareinfo.xml \
	vendor/gnosis/prebuilt/etc/permissions/com.google.android.maps.xml:system/etc/permissions/com.google.android.maps.xml \
	vendor/gnosis/prebuilt/etc/permissions/com.google.android.media.effects.xml:system/etc/permissions/com.google.android.media.effects.xml \
	vendor/gnosis/prebuilt/etc/permissions/com.google.android.tv.installed.xml:system/etc/permissions/com.google.android.tv.installed.xml \
	vendor/gnosis/prebuilt/etc/permissions/com.google.hardware.camera.easel_2018.xml:system/etc/permissions/com.google.hardware.camera.easel_2018.xml \
	vendor/gnosis/prebuilt/etc/permissions/com.google.vr.platform.xml:system/etc/permissions/com.google.vr.platform.xml \
	vendor/gnosis/prebuilt/etc/permissions/com.google.widevine.software.drm.xml:system/etc/permissions/com.google.widevine.software.drm.xml \
	vendor/gnosis/prebuilt/etc/permissions/privapp-permissions-google.xml:system/etc/permissions/privapp-permissions-google.xml \
	vendor/gnosis/prebuilt/etc/permissions/turbo.xml:system/etc/permissions/turbo.xml \
	vendor/gnosis/prebuilt/etc/preferred-apps/google.xml:system/etc/preferred-apps/google.xml \
	vendor/gnosis/prebuilt/etc/sysconfig/dialer_experience.xml:system/etc/sysconfig/dialer_experience.xml \
	vendor/gnosis/prebuilt/etc/sysconfig/framework-sysconfig.xml:system/etc/sysconfig/framework-sysconfig.xml \
	vendor/gnosis/prebuilt/etc/sysconfig/google.xml:system/etc/sysconfig/google.xml \
	vendor/gnosis/prebuilt/etc/sysconfig/google_build.xml:system/etc/sysconfig/google_build.xml \
	vendor/gnosis/prebuilt/etc/sysconfig/google_exclusives_enable.xml:system/etc/sysconfig/google_exclusives_enable.xml \
	vendor/gnosis/prebuilt/etc/sysconfig/google-hiddenapi-package-whitelist.xml:system/etc/sysconfig/google-hiddenapi-package-whitelist.xml \
	vendor/gnosis/prebuilt/etc/sysconfig/google_vr_build.xml:system/etc/sysconfig/google_vr_build.xml \
	vendor/gnosis/prebuilt/etc/sysconfig/hiddenapi-package-whitelist.xml:system/etc/sysconfig/hiddenapi-package-whitelist.xml \
	vendor/gnosis/prebuilt/etc/sysconfig/nexus.xml:system/etc/sysconfig/nexus.xml \
	vendor/gnosis/prebuilt/etc/sysconfig/whitelist_com.android.omadm.service.xml:system/etc/sysconfig/whitelist_com.android.omadm.service.xml

# Framework
PRODUCT_COPY_FILES += \
	vendor/gnosis/prebuilt/framework/arm/boot-com.google.vr.platform.art:system/framework/arm/boot-com.google.vr.platform.art \
	vendor/gnosis/prebuilt/framework/arm/boot-com.google.vr.platform.art.rel:system/framework/arm/boot-com.google.vr.platform.art.rel \
	vendor/gnosis/prebuilt/framework/arm/boot-com.google.vr.platform.oat:system/framework/arm/boot-com.google.vr.platform.oat \
	vendor/gnosis/prebuilt/framework/arm/boot-com.google.vr.platform.vdex:system/framework/arm/boot-com.google.vr.platform.vdex \
	vendor/gnosis/prebuilt/framework/arm64/boot-com.google.vr.platform.art:system/framework/arm64/boot-com.google.vr.platform.art \
	vendor/gnosis/prebuilt/framework/arm64/boot-com.google.vr.platform.art.rel:system/framework/arm64/boot-com.google.vr.platform.art.rel \
	vendor/gnosis/prebuilt/framework/arm64/boot-com.google.vr.platform.oat:system/framework/arm64/boot-com.google.vr.platform.oat \
	vendor/gnosis/prebuilt/framework/arm64/boot-com.google.vr.platform.vdex:system/framework/arm64/boot-com.google.vr.platform.vdex \
	vendor/gnosis/prebuilt/framework/oat/arm/com.google.android.camera.experimental2018.odex:system/framework/oat/arm/com.google.android.camera.experimental2018.odex \
	vendor/gnosis/prebuilt/framework/oat/arm/com.google.android.camera.experimental2018.vdex:system/framework/oat/arm/com.google.android.camera.experimental2018.vdex \
	vendor/gnosis/prebuilt/framework/oat/arm/com.google.android.dialer.support.odex:system/framework/oat/arm/com.google.android.dialer.support.odex \
	vendor/gnosis/prebuilt/framework/oat/arm/com.google.android.dialer.support.vdex:system/framework/oat/arm/com.google.android.dialer.support.vdex \
	vendor/gnosis/prebuilt/framework/oat/arm/com.google.android.maps.odex:system/framework/oat/arm/com.google.android.maps.odex \
	vendor/gnosis/prebuilt/framework/oat/arm/com.google.android.maps.vdex:system/framework/oat/arm/com.google.android.maps.vdex \
	vendor/gnosis/prebuilt/framework/oat/arm/com.google.android.media.effects.odex:system/framework/oat/arm/com.google.android.media.effects.odex \
	vendor/gnosis/prebuilt/framework/oat/arm/com.google.android.media.effects.vdex:system/framework/oat/arm/com.google.android.media.effects.vdex \
	vendor/gnosis/prebuilt/framework/oat/arm64/com.google.android.camera.experimental2018.odex:system/framework/oat/arm64/com.google.android.camera.experimental2018.odex \
	vendor/gnosis/prebuilt/framework/oat/arm64/com.google.android.camera.experimental2018.vdex:system/framework/oat/arm64/com.google.android.camera.experimental2018.vdex \
	vendor/gnosis/prebuilt/framework/oat/arm64/com.google.android.dialer.support.odex:system/framework/oat/arm64/com.google.android.dialer.support.odex \
	vendor/gnosis/prebuilt/framework/oat/arm64/com.google.android.dialer.support.vdex:system/framework/oat/arm64/com.google.android.dialer.support.vdex \
	vendor/gnosis/prebuilt/framework/oat/arm64/com.google.android.maps.odex:system/framework/oat/arm64/com.google.android.maps.odex \
	vendor/gnosis/prebuilt/framework/oat/arm64/com.google.android.maps.vdex:system/framework/oat/arm64/com.google.android.maps.vdex \
	vendor/gnosis/prebuilt/framework/oat/arm64/com.google.android.media.effects.odex:system/framework/oat/arm64/com.google.android.media.effects.odex \
	vendor/gnosis/prebuilt/framework/oat/arm64/com.google.android.media.effects.vdex:system/framework/oat/arm64/com.google.android.media.effects.vdex \
	vendor/gnosis/prebuilt/framework/com.google.android.camera.experimental2016.jar:system/framework/com.google.android.camera.experimental2016.jar \
	vendor/gnosis/prebuilt/framework/com.google.android.dialer.support.jar:system/framework/com.google.android.dialer.support.jar \
	vendor/gnosis/prebuilt/framework/com.google.android.maps.jar:system/framework/com.google.android.maps.jar \
	vendor/gnosis/prebuilt/framework/com.google.android.media.effects.jar:system/framework/com.google.android.media.effects.jar \
	vendor/gnosis/prebuilt/framework/com.google.vr.platform.jar:system/framework/com.google.vr.platform.jar \
	vendor/gnosis/prebuilt/framework/com.google.widevine.software.drm.jar:system/framework/com.google.widevine.software.drm.jar

# Lib
PRODUCT_COPY_FILES += \
	vendor/gnosis/prebuilt/lib/libdl.so:system/lib/libdl.so \
	vendor/gnosis/prebuilt/lib/libdmengine.so:system/lib/libdmengine.so \
	vendor/gnosis/prebuilt/lib/libdmjavaplugin.so:system/lib/libdmjavaplugin.so \
	vendor/gnosis/prebuilt/lib/libdvr.so:system/lib/libdvr.so \
	vendor/gnosis/prebuilt/lib/libdvr_loader.so:system/lib/libdvr_loader.so \
	vendor/gnosis/prebuilt/lib/libeaselcomm.so:system/lib/libeaselcomm.so \
	vendor/gnosis/prebuilt/lib/libeaselcontrol.amber.so:system/lib/libeaselcontrol.amber.so \
	vendor/gnosis/prebuilt/lib/libfilterpack_facedetect.so:system/lib/libfilterpack_facedetect.so \
	vendor/gnosis/prebuilt/lib/libfilterpack_imageproc.so:system/lib/libfilterpack_imageproc.so \
	vendor/gnosis/prebuilt/lib/libfrsdk.so:system/lib/libfrsdk.so \
	vendor/gnosis/prebuilt/lib/libprotobuf-cpp-full.so:system/lib/libprotobuf-cpp-full.so \
	vendor/gnosis/prebuilt/lib/libprotobuf-cpp-lite.so:system/lib/libprotobuf-cpp-lite.so \
	vendor/gnosis/prebuilt/lib/libsketchology_native.so:system/lib/libsketchology_native.so

#Lib64
PRODUCT_COPY_FILES += \
	vendor/gnosis/prebuilt/lib64/libbarhopper.so:system/lib64/libbarhopper.so \
	vendor/gnosis/prebuilt/lib64/libdl.so:system/lib64/libdl.so \
	vendor/gnosis/prebuilt/lib64/libdvr.so:system/lib64/libdvr.so \
	vendor/gnosis/prebuilt/lib64/libdvr_loader.so:system/lib64/libdvr_loader.so \
	vendor/gnosis/prebuilt/lib64/libeaselcomm.so:system/lib64/libeaselcomm.so \
	vendor/gnosis/prebuilt/lib64/libeaselcontrol.amber.so:system/lib64/libeaselcontrol.amber.so \
	vendor/gnosis/prebuilt/lib64/libfacenet.so:system/lib64/libfacenet.so \
	vendor/gnosis/prebuilt/lib64/libfilterpack_facedetect.so:system/lib64/libfilterpack_facedetect.so \
	vendor/gnosis/prebuilt/lib64/libfilterpack_imageproc.so:system/lib64/libfilterpack_imageproc.so \
	vendor/gnosis/prebuilt/lib64/libfrsdk.so:system/lib64/libfrsdk.so \
	vendor/gnosis/prebuilt/lib64/libgdx.so:system/lib64/libgdx.so \
	vendor/gnosis/prebuilt/lib64/libjni_latinime.so:system/lib64/libjni_latinime.so \
	vendor/gnosis/prebuilt/lib64/libjni_latinimegoogle.so:system/lib64/libjni_latinimegoogle.so \
	vendor/gnosis/prebuilt/lib64/liblpmdeviceutils.so:system/lib64/liblpmdeviceutils.so \
	vendor/gnosis/prebuilt/lib64/libsketchology_native.so:system/lib64/libsketchology_native.so \
	vendor/gnosis/prebuilt/lib64/libvr_hwc-hal.so:system/lib64/libvr_hwc-hal.so \
	vendor/gnosis/prebuilt/lib64/libwallpapers-breel-jni.so:system/lib64/libwallpapers-breel-jni.so

# Usr
PRODUCT_COPY_FILES += \
	vendor/gnosis/prebuilt/usr/srec/en-US/am_phonemes.syms:system/usr/srec/en-US/am_phonemes.syms \
	vendor/gnosis/prebuilt/usr/srec/en-US/app_bias.fst:system/usr/srec/en-US/app_bias.fst \
	vendor/gnosis/prebuilt/usr/srec/en-US/APP_NAME.fst:system/usr/srec/en-US/APP_NAME.fst \
	vendor/gnosis/prebuilt/usr/srec/en-US/APP_NAME.syms:system/usr/srec/en-US/APP_NAME.syms \
	vendor/gnosis/prebuilt/usr/srec/en-US/c_fst:system/usr/srec/en-US/c_fst \
	vendor/gnosis/prebuilt/usr/srec/en-US/CLG.prewalk.fst:system/usr/srec/en-US/CLG.prewalk.fst \
	vendor/gnosis/prebuilt/usr/srec/en-US/commands.abnf:system/usr/srec/en-US/commands.abnf \
	vendor/gnosis/prebuilt/usr/srec/en-US/compile_grammar.config:system/usr/srec/en-US/compile_grammar.config \
	vendor/gnosis/prebuilt/usr/srec/en-US/config.pumpkin:system/usr/srec/en-US/config.pumpkin \
	vendor/gnosis/prebuilt/usr/srec/en-US/confirmation_bias.fst:system/usr/srec/en-US/confirmation_bias.fst \
	vendor/gnosis/prebuilt/usr/srec/en-US/CONTACT_NAME.fst:system/usr/srec/en-US/CONTACT_NAME.fst \
	vendor/gnosis/prebuilt/usr/srec/en-US/CONTACT_NAME.syms:system/usr/srec/en-US/CONTACT_NAME.syms \
	vendor/gnosis/prebuilt/usr/srec/en-US/contacts.abnf:system/usr/srec/en-US/contacts.abnf \
	vendor/gnosis/prebuilt/usr/srec/en-US/contacts_bias.fst:system/usr/srec/en-US/contacts_bias.fst \
	vendor/gnosis/prebuilt/usr/srec/en-US/contacts_disambig.fst:system/usr/srec/en-US/contacts_disambig.fst \
	vendor/gnosis/prebuilt/usr/srec/en-US/dict:system/usr/srec/en-US/dict \
	vendor/gnosis/prebuilt/usr/srec/en-US/dictation.config:system/usr/srec/en-US/dictation.config \
	vendor/gnosis/prebuilt/usr/srec/en-US/dnn:system/usr/srec/en-US/dnn \
	vendor/gnosis/prebuilt/usr/srec/en-US/embedded_class_denorm.mfar:system/usr/srec/en-US/embedded_class_denorm.mfar \
	vendor/gnosis/prebuilt/usr/srec/en-US/embedded_normalizer.mfar:system/usr/srec/en-US/embedded_normalizer.mfar \
	vendor/gnosis/prebuilt/usr/srec/en-US/endpointer_dictation.config:system/usr/srec/en-US/endpointer_dictation.config \
	vendor/gnosis/prebuilt/usr/srec/en-US/endpointer_model:system/usr/srec/en-US/endpointer_model \
	vendor/gnosis/prebuilt/usr/srec/en-US/endpointer_model.mmap:system/usr/srec/en-US/endpointer_model.mmap \
	vendor/gnosis/prebuilt/usr/srec/en-US/endpointer_voicesearch.config:system/usr/srec/en-US/endpointer_voicesearch.config \
	vendor/gnosis/prebuilt/usr/srec/en-US/ep_portable_mean_stddev:system/usr/srec/en-US/ep_portable_mean_stddev \
	vendor/gnosis/prebuilt/usr/srec/en-US/ep_portable_model.uint8.mmap:system/usr/srec/en-US/ep_portable_model.uint8.mmap \
	vendor/gnosis/prebuilt/usr/srec/en-US/g2p.data:system/usr/srec/en-US/g2p.data \
	vendor/gnosis/prebuilt/usr/srec/en-US/g2p_fst:system/usr/srec/en-US/g2p_fst \
	vendor/gnosis/prebuilt/usr/srec/en-US/g2p_graphemes.syms:system/usr/srec/en-US/g2p_graphemes.syms \
	vendor/gnosis/prebuilt/usr/srec/en-US/g2p_phonemes.syms:system/usr/srec/en-US/g2p_phonemes.syms \
	vendor/gnosis/prebuilt/usr/srec/en-US/grammar.config:system/usr/srec/en-US/grammar.config \
	vendor/gnosis/prebuilt/usr/srec/en-US/hmmlist:system/usr/srec/en-US/hmmlist \
	vendor/gnosis/prebuilt/usr/srec/en-US/hmm_symbols:system/usr/srec/en-US/hmm_symbols \
	vendor/gnosis/prebuilt/usr/srec/en-US/input_mean_std_dev:system/usr/srec/en-US/input_mean_std_dev \
	vendor/gnosis/prebuilt/usr/srec/en-US/lexicon.U.fst:system/usr/srec/en-US/lexicon.U.fst \
	vendor/gnosis/prebuilt/usr/srec/en-US/lstm_model.uint8.data:system/usr/srec/en-US/lstm_model.uint8.data \
	vendor/gnosis/prebuilt/usr/srec/en-US/magic_mic.config:system/usr/srec/en-US/magic_mic.config \
	vendor/gnosis/prebuilt/usr/srec/en-US/media_bias.fst:system/usr/srec/en-US/media_bias.fst \
	vendor/gnosis/prebuilt/usr/srec/en-US/metadata:system/usr/srec/en-US/metadata \
	vendor/gnosis/prebuilt/usr/srec/en-US/monastery_config.pumpkin:system/usr/srec/en-US/monastery_config.pumpkin \
	vendor/gnosis/prebuilt/usr/srec/en-US/norm_fst:system/usr/srec/en-US/norm_fst \
	vendor/gnosis/prebuilt/usr/srec/en-US/offensive_word_normalizer.mfar:system/usr/srec/en-US/offensive_word_normalizer.mfar \
	vendor/gnosis/prebuilt/usr/srec/en-US/offline_action_data.pb:system/usr/srec/en-US/offline_action_data.pb \
	vendor/gnosis/prebuilt/usr/srec/en-US/phonelist:system/usr/srec/en-US/phonelist \
	vendor/gnosis/prebuilt/usr/srec/en-US/portable_lstm:system/usr/srec/en-US/portable_lstm \
	vendor/gnosis/prebuilt/usr/srec/en-US/portable_meanstddev:system/usr/srec/en-US/portable_meanstddev \
	vendor/gnosis/prebuilt/usr/srec/en-US/pumpkin.mmap:system/usr/srec/en-US/pumpkin.mmap \
	vendor/gnosis/prebuilt/usr/srec/en-US/read_items_bias.fst:system/usr/srec/en-US/read_items_bias.fst \
	vendor/gnosis/prebuilt/usr/srec/en-US/rescoring.fst.compact:system/usr/srec/en-US/rescoring.fst.compact \
	vendor/gnosis/prebuilt/usr/srec/en-US/semantics.pumpkin:system/usr/srec/en-US/semantics.pumpkin \
	vendor/gnosis/prebuilt/usr/srec/en-US/skip_items_bias.fst:system/usr/srec/en-US/skip_items_bias.fst \
	vendor/gnosis/prebuilt/usr/srec/en-US/SONG_NAME.fst:system/usr/srec/en-US/SONG_NAME.fst \
	vendor/gnosis/prebuilt/usr/srec/en-US/SONG_NAME.syms:system/usr/srec/en-US/SONG_NAME.syms \
	vendor/gnosis/prebuilt/usr/srec/en-US/time_bias.fst:system/usr/srec/en-US/time_bias.fst \
	vendor/gnosis/prebuilt/usr/srec/en-US/transform.mfar:system/usr/srec/en-US/transform.mfar \
	vendor/gnosis/prebuilt/usr/srec/en-US/voice_actions.config:system/usr/srec/en-US/voice_actions_compiler.config \
	vendor/gnosis/prebuilt/usr/srec/en-US/voice_actions_compiler.config:system/usr/srec/en-US/voice_actions_compiler.config \
	vendor/gnosis/prebuilt/usr/srec/en-US/word_confidence_classifier:system/usr/srec/en-US/word_confidence_classifier \
	vendor/gnosis/prebuilt/usr/srec/en-US/wordlist.syms:system/usr/srec/en-US/wordlist.syms

# Gnosis Launcher
PRODUCT_COPY_FILES += \
	vendor/gnosis/prebuilt/priv-app/GnosisLauncher/GnosisLauncher.apk:system/priv-app/GnosisLauncher/GnosisLauncher.apk

# Busybox
PRODUCT_COPY_FILES += \
	vendor/gnosis/prebuilt/xbin/busybox:system/xbin/busybox


